#
# THIS FILE IS GENERATED, DO NOT EDIT

################################################################################
#
# Centos checks
#
################################################################################

#
# Common variable definitions
#
.ci-commons-centos:
  variables:
    FDO_DISTRIBUTION_PACKAGES: 'wget curl'
    FDO_DISTRIBUTION_EXEC: './test/script.sh'
    FDO_DISTRIBUTION_VERSION: '7'
    FDO_DISTRIBUTION_TAG: fdo-ci-$CI_PIPELINE_ID
    FDO_EXPIRES_AFTER: '1h'
  needs:
    - bootstrap
    - sanity check

#
# A few templates to avoid writing the image and stage in each job
#
.centos:ci@container-build:
  extends:
    - .fdo.container-build@centos
    - .ci-commons-centos
  image: $CI_REGISTRY_IMAGE/buildah:2020-07-20.1
  stage: centos_container_build


#
# generic centos checks
#
.centos@check:
  extends:
    - .fdo.distribution-image@centos
    - .ci-commons-centos
  stage: centos_check
  script:
      # run both curl and wget because one of those two is installed and one is
      # in the base image, but it depends on the distro which one
    - curl --insecure https://gitlab.freedesktop.org
    - wget --no-check-certificate https://gitlab.freedesktop.org
      # make sure our test script has been run
    - if [[ -e /test_file ]] ;
      then
        echo $FDO_DISTRIBUTION_EXEC properly run ;
      else
        exit 1 ;
      fi


#
# straight centos build and test
#
centos:7@container-build:
  extends: .centos:ci@container-build


centos:7@check:
  extends: .centos@check
  needs:
    - centos:7@container-build
    - sanity check

# Test FDO_BASE_IMAGE. We don't need to do much here, if our
# FDO_DISTRIBUTION_EXEC script can run curl+wget this means we're running on
# the desired base image. That's good enough.
centos:7@base-image:
  extends: centos:7@container-build
  stage: centos_check
  variables:
    # We need to duplicate FDO_DISTRIBUTION_TAG here, gitlab doesn't allow nested expansion
    FDO_BASE_IMAGE: registry.freedesktop.org/$CI_PROJECT_PATH/centos/7:fdo-ci-$CI_PIPELINE_ID
    FDO_DISTRIBUTION_PACKAGES: ''
    FDO_DISTRIBUTION_EXEC: 'test/test-wget-curl.sh'
    FDO_FORCE_REBUILD: 1
    FDO_DISTRIBUTION_TAG: fdo-ci-baseimage-$CI_PIPELINE_ID
  needs:
    - centos:7@container-build
    - sanity check

#
# /cache centos check (in build stage)
#
# Also ensures setting FDO_FORCE_REBUILD will do the correct job
#
centos@cache-container-build:
  extends: .centos:ci@container-build
  before_script:
      # The template normally symlinks the /cache
      # folder, but we want a fresh new one for the
      # tests.
    - mkdir runner_cache_$CI_PIPELINE_ID
    - uname -a | tee runner_cache_$CI_PIPELINE_ID/foo-$CI_PIPELINE_ID

  artifacts:
    paths:
      - runner_cache_$CI_PIPELINE_ID/*
    expire_in: 1 week

  variables:
    FDO_DISTRIBUTION_TAG: fdo-ci-cache-$CI_PIPELINE_ID
    FDO_DISTRIBUTION_EXEC: 'bash test/test_cache.sh $CI_PIPELINE_ID'
    FDO_CACHE_DIR: $CI_PROJECT_DIR/runner_cache_$CI_PIPELINE_ID
    FDO_FORCE_REBUILD: 1

#
# /cache centos check (in check stage)
#
centos@cache-check:
  stage: centos_check
  image: alpine:latest
  script:
    # in the previous stage (centos@cache-container-build),
    # test/test_cache.sh checked for the existance of `/cache/foo-$CI_PIPELINE_ID`
    # and if it found it, it wrote `/cache/bar-$CI_PIPELINE_ID`.
    #
    # So if we have in the artifacts `bar-$CI_PIPELINE_ID`, that means
    # 2 things:
    # - /cache was properly mounted while building the container
    # - the $FDO_CACHE_DIR has been properly written from within the
    #   building container, meaning the /cache folder has been successfully
    #   updated.
    - if [ -e $CI_PROJECT_DIR/runner_cache_$CI_PIPELINE_ID/bar-$CI_PIPELINE_ID ] ;
      then
        echo Successfully read/wrote the cache folder, all good ;
      else
        echo FAILURE while retrieving the previous artifacts ;
        exit 1 ;
      fi
  needs:
    - job: centos@cache-container-build
      artifacts: true
    - sanity check


#
# make sure we do not rebuild the image if the tag exists (during the check)
#
do not rebuild centos:7@container-build:
  extends: .centos:ci@container-build
  stage: centos_check
  variables:
    FDO_UPSTREAM_REPO: $CI_PROJECT_PATH
    FDO_DISTRIBUTION_PACKAGES: 'this-package-should-not-exist'
  needs:
    - centos:7@container-build
    - sanity check


#
# check if the labels were correctly applied
#
check labels centos:7:
  extends:
    - centos:7@check
  image: $CI_REGISTRY_IMAGE/buildah:2020-07-20.1
  script:
    # FDO_DISTRIBUTION_IMAGE still has indirections
    - DISTRO_IMAGE=$(eval echo ${FDO_DISTRIBUTION_IMAGE})

    # retrieve the infos from the registry (once)
    - JSON_IMAGE=$(skopeo inspect docker://$DISTRO_IMAGE)

    # parse all the labels we care about
    - IMAGE_PIPELINE_ID=$(echo $JSON_IMAGE | jq -r '.Labels["fdo.pipeline_id"]')
    - IMAGE_JOB_ID=$(echo $JSON_IMAGE | jq -r '.Labels["fdo.job_id"]')
    - IMAGE_PROJECT=$(echo $JSON_IMAGE | jq -r '.Labels["fdo.project"]')
    - IMAGE_COMMIT=$(echo $JSON_IMAGE | jq -r '.Labels["fdo.commit"]')

    # some debug information
    - echo $JSON_IMAGE
    - echo $IMAGE_PIPELINE_ID $CI_PIPELINE_ID
    - echo $IMAGE_JOB_ID
    - echo $IMAGE_PROJECT $CI_PROJECT_PATH
    - echo $IMAGE_COMMIT $CI_COMMIT_SHA

    # ensure the labels are correct (we are on the same pipeline)
    - '[[ x"$IMAGE_PIPELINE_ID" == x"$CI_PIPELINE_ID" ]]'
    - '[[ x"$IMAGE_JOB_ID" != x"" ]]' # we don't know the job ID, but it must be set
    - '[[ x"$IMAGE_PROJECT" == x"$CI_PROJECT_PATH" ]]'
    - '[[ x"$IMAGE_COMMIT" == x"$CI_COMMIT_SHA" ]]'
  needs:
    - centos:7@container-build
    - sanity check


#
# make sure we do not rebuild the image if the tag exists in the upstream
# repository (during the check)
# special case where FDO_REPO_SUFFIX == ci_templates_test_upstream
#
pull upstream centos:7@container-build:
  extends: .centos:ci@container-build
  stage: centos_check
  variables:
    FDO_UPSTREAM_REPO: $CI_PROJECT_PATH
    FDO_REPO_SUFFIX: centos/ci_templates_test_upstream
    FDO_DISTRIBUTION_PACKAGES: 'this-package-should-not-exist'
  needs:
    - centos:7@container-build
    - sanity check

#
# Try our centos scripts with other versions and check
#

centos:8@container-build:
  extends: .centos:ci@container-build
  variables:
    FDO_DISTRIBUTION_VERSION: '8'

centos:8@check:
  extends: .centos@check
  variables:
    FDO_DISTRIBUTION_VERSION: '8'
  needs:
    - centos:8@container-build
    - sanity check
